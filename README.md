# pinball

An Open Source Congestion Management layer using permissioned Oracles. Enables a permissionless byzantine fault tolerant system over a authorized layer.
